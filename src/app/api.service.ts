import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';

const API_URL = environment.apiUrl;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'text/xml'})
}
;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {
  }

  postProcess(xmlRequest: string, processType: string) {
    switch (processType) {
      case 'listaprocessi':
        this.http.post(`${API_URL}/listaprocessi`, xmlRequest, httpOptions).subscribe((d) => console.log('done', d),
          err => console.error(err)
        );
        break;
    }
  }
}
