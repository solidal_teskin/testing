import {Component, OnInit} from '@angular/core';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  menuItemList = [
    {
      id: 0,
      name: 'Processi',
      navLink: '/process',
      isSelected: 'active'
    },
    {
      id: 1,
      name: 'Regole',
      navLink: '#',
      isSelected: ''
    },
    {
      id: 2,
      name: 'Verifica ordine',
      navLink: '#',
      isSelected: ''
    },
    {
      id: 3,
      name: 'Prodotti vendibili',
      navLink: '#',
      isSelected: ''
    }
  ];

  onSelect(index) {
    this.menuItemList.forEach(menuItem => this.menuItemList[menuItem.id].isSelected = '');
    this.menuItemList[index].isSelected = 'active';
  }

  constructor() {
  }

  ngOnInit() {
  }

}
