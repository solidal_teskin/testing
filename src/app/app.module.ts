import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SideBarComponent} from './core/side-bar/side-bar.component';
import { HttpRequestComponent } from './components/http-request/http-request.component';
import { FormComponent } from './components/form/form.component';
import { HttpResponseComponent } from './components/http-response/http-response.component';
import { MainLayoutComponent } from './core/main-layout/main-layout.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [{
  path: 'process', component: FormComponent
}];

@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    HttpRequestComponent,
    FormComponent,
    HttpResponseComponent,
    MainLayoutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,
      {enableTracing: true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
