import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private bankSource = new BehaviorSubject('');
  private productCodeSource = new BehaviorSubject('');
  private policyNumberSource = new BehaviorSubject('');
  private propCompNumberSource = new BehaviorSubject('');

  currentBank = this.bankSource.asObservable();
  currentProductCode = this.productCodeSource.asObservable();
  currentPolicyNumber = this.policyNumberSource.asObservable();
  currentPropCompNumber = this.propCompNumberSource.asObservable();


  constructor() { }

  updateBank(currentBank: string) {
    this.bankSource.next(currentBank);
  }
  updateProductCode(productCode: string) {
    this.productCodeSource.next(productCode);
  }
  updatePolicyNumber(policyNumber: string) {
    this.policyNumberSource.next(policyNumber);
  }
  updatePropCompNumber(propCompNumber: string) {
    this.propCompNumberSource.next(propCompNumber);
  }
}
