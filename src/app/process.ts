export class Process {
  id: number;
  bank: string;
  productCode: string;
  policyNumber: string;
  propCompNumber: string;
}
