import {Component, Input, OnInit} from '@angular/core';
import {Process} from '../../model/process';
import {DataService} from '../../service/data.service';
import {ApiService} from '../../api.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  constructor(private data: DataService, private _apiService: ApiService) {
  }

  process = new Process();

  ngOnInit() {
    this.data.currentBank.subscribe(bank => this.process.bank = bank);
    this.data.currentProductCode.subscribe(productCode => this.process.productCode = productCode);
    this.data.currentPolicyNumber.subscribe(policyNumber => this.process.policyNumber = policyNumber);
    this.data.currentPropCompNumber.subscribe(propCompNumber => this.process.propCompNumber = propCompNumber);
  }

  onInputChangeBank(bank: string) {
    this.data.updateBank(bank);
  }

  onInputChangeProductCode(productCode: string) {
    this.data.updateProductCode(productCode);
  }

  onInputChangePolicyNumber(policyNumber: string) {
    this.data.updatePolicyNumber(policyNumber);
  }

  onInputChangePropCompNumber(propCompNumber: string) {
    this.data.updatePropCompNumber(propCompNumber);
  }

  createXmlProcessi(processType: string) {
    switch (processType) {
      case 'listaprocessi':
        return `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                 <requestModel>
                    <banca>${this.process.bank}</banca>
                    <codiceProdotto>${this.process.productCode}</codiceProdotto>
                    <numeroPolizza>${this.process.policyNumber}</numeroPolizza>
                    <numeroPropComp>${this.process.propCompNumber}</numeroPropComp>
                 </requestModel>`;
    }
  }

  onFormSubmitEvent() {
    this._apiService.postProcess(this.createXmlProcessi('listaprocessi'), 'listaprocessi');
  }
}
