import {Component, Input, OnInit} from '@angular/core';
import {DataService} from '../../service/data.service';
import {Process} from '../../model/process';

@Component({
  selector: 'app-http-request',
  templateUrl: './http-request.component.html',
  styleUrls: ['./http-request.component.css']
})
export class HttpRequestComponent implements OnInit {

  constructor(private data: DataService) {  }
  // process: Process;
  process = new Process();

  ngOnInit() {
    this.data.currentBank.subscribe(bank => this.process.bank = bank);
    this.data.currentProductCode.subscribe(productCode => this.process.productCode = productCode);
    this.data.currentPolicyNumber.subscribe(policyNumber => this.process.policyNumber = policyNumber);
    this.data.currentPropCompNumber.subscribe(propCompNumber => this.process.propCompNumber = propCompNumber);
  }

}
